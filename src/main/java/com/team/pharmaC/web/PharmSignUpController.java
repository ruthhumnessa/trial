package com.team.pharmaC.web;

import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.team.pharmaC.Pharmacy;
import com.team.pharmaC.repositories.PharmacyRepository;
import lombok.extern.slf4j.Slf4j;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller


public class PharmSignUpController {
	
	private PharmacyRepository pharmacyRepository;
	public PharmSignUpController(PharmacyRepository pharmacyRepository) {
		this.pharmacyRepository = pharmacyRepository;
	}
  
	
	@ModelAttribute("pharmacy")
	public Pharmacy pharmacy() {
		return new Pharmacy();
	}
	
	
  @RequestMapping("/pharmacy")
  public String greetingForm() {
	  return "pharmacy";    
  }
  

	@PostMapping("pharmacy")
	public String processOrder(@Valid @ModelAttribute("pharmacy") Pharmacy pharmacy, Errors errors, Model model) {
		
		if(errors.hasErrors()) {
			return "pharmacy";
		}
		else if(!pharmacy.getPassword().equals(pharmacy.getRePassword())) {
			model.addAttribute("pwderr" , "**The two password must match");
			return "pharmacy";
		}
		Pharmacy savePharmacy = pharmacyRepository.save(pharmacy);
		//Log.info("Pharmacy registered: " + savePharmacy);
		return "registeredPharmacy";
	}

}
