package com.team.pharmaC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PharmaCapplication {

	public static void main(String[] args) {
		SpringApplication.run(PharmaCapplication.class, args);
	}
}
